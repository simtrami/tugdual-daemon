@extends('layout')

@section('title', 'Tugdual Daemon')

@section('content')
    <div class="title m-b-md">
        <h1>Tugdual Daemon</h1>
        <h2 class="subtitle">An open-source URL unshortener (for a friend).</h2>
    </div>

    <form action="{{ route('form') }}" method="POST">
        @csrf
        <label for="url">URL to unshorten</label>
        <input id="url" type="text" class="@error('url') is-invalid @enderror" placeholder="http://"
               name="url" value="{{ old('url') }}">

        <button type="submit">Do the thing</button>

        @error('url')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </form>

    @isset($result)
        <div class="result">
            <div class="alert alert-{{ $result->getStatus() === 200 ? 'success' : 'warning' }}">
                Request resulted with a <em>{{ $result->getStatus() }}</em> status code.<br/>
                Unshortened URL: <a href="{{ $result->getUrl() }}" target="_blank">{{ $result->getUrl() }}</a>
            </div>
            <div id="trace">
                Trace ({{ $result->getRedirections() }} redirection{{ $result->getRedirections() > 1 ? 's' : '' }}):
                <ol>
                    @foreach($result->getTrace() as $index => $url)
                        <li><strong>{{ $index }}.</strong> <a href="{{ $url }}" target="_blank">{{ $url }}</a></li>
                    @endforeach
                </ol>
            </div>
        </div>
    @endisset
@endsection
