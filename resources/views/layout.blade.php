<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="app">
    <div class="flex-center position-ref full-height">
        <div class="content">
            @yield('content')
        </div>
    </div>

    <div id="footer">
        <p>
            Made with &#x1F496; by <a href="https://gitlab.com/simtrami" target="_blank">Simtrami</a><br/>
            Check it out on <a href="https://gitlab.com/simtrami/tugdual-daemon" target="_blank">Gitlab</a>!
        </p>
    </div>
</div>
</body>
</html>
