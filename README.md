# Tugdual Daemon

An URL unshortener, for a friend.

## Installation

I am developing this tool with the wonderful Laravel PHP framework, version 7.x
because it has an integrated HTTP client.
Therefore, everything you need to know about installation and deployment can
be found in the [official documentation](https://laravel.com/docs/7.x/).

## Usage

You play with it locally via the dedicated [Artisan command](https://laravel.com/docs/7.x/artisan)
`unshorten:url {url}`, however there must be an easier way to integrate such a tool in command line.

Otherwise, you can deploy your own instance (see [documentation](https://laravel.com/docs/7.x/deployment))
or use mine at the following address :
[https://tugdual-daemon.simtrami.net](https://tugdual-daemon.simtrami.net)

## Contribution

Contributions are open, though I do not plan on supporting it for too long
as the idea emanated during a boring afternoon.

I would be glad to integrate your merge requests, and will do my best to review them shortly.
