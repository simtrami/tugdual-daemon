<?php

namespace App\Http\Controllers;

use App\UnshortenedURL;
use GuzzleHttp\Exception\TransferException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UnshortenController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('index')->with('result', session()->get('result'));
    }

    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function form(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'url' => 'required|url',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        }

        $url = $request->input('url');

        try {
            $unshortenedUrl = $this->unshorten($url);
        } catch (BadRequestHttpException $e) {
            $validator->errors()->add('url', "The request ended with an error. Try again.");
            return redirect('/')->withErrors($validator)->withInput();
        }

        return redirect('/')->with('result', $unshortenedUrl);
    }

    /**
     * @param String $url
     * @return UnshortenedURL
     */
    private function unshorten(string $url): UnshortenedURL
    {
        $location = new Collection($url);

        $client = Http::withoutRedirecting();

        $status = null;

        do {
            try {
                $response = $client->get($location->last());
            } catch (TransferException $exception) {
                throw new BadRequestHttpException("The given URL is not processable: {$location->last()}");
            }
            $status = $response->status();
            $location[] = $response->header('Location');
        } while ($status >= 301 and $status <= 308 and !empty($location->last()));

        $location->pop();
        return new UnshortenedURL($location->last(), $location, $location->count() - 1, $status);
    }
}
