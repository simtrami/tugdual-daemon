<?php

namespace App\Console\Commands;

use App\UnshortenedURL;
use GuzzleHttp\Exception\TransferException;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class UnshortenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unshorten:url {url? : URL to unshorten}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Unshorten a given URL.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $location = new Collection();
        $location[] = $this->argument('url') ?: $this->ask('URL to unshorten: ');

        $client = Http::withoutRedirecting();

        $status = null;

        do {
            print_r(($status ? "[" . ($location->count() - 1) . "] Status: {$status} - " : "") . "Location: {$location->last()}\n");
            try {
                $response = $client->get($location->last());
            } catch (TransferException $exception) {
                $this->error("\n" . $exception);
                return 1;
            }
            $status = $response->status();
            $location[] = $response->header('Location');
        } while ($status >= 301 and $status <= 308 and !empty($location->last()));

        $location->pop();
        $unshortenedUrl = new UnshortenedURL($location->last(), $location, $location->count() - 1, $status);
        if ($status === 200) {
            $this->info("\n" . $unshortenedUrl);
        } else {
            $this->error("\n" . $unshortenedUrl);
        }

        return 0;
    }
}
