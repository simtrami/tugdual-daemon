<?php


namespace App;


use Illuminate\Support\Collection;

class UnshortenedURL
{
    private $url;
    private $trace;
    private $redirections;
    private $status;

    /**
     * UnshortenedURL constructor.
     * @param String $url
     * @param Collection $trace
     * @param int $redirections
     * @param int $status
     */
    public function __construct(String $url, Collection $trace, int $redirections, int $status = 200)
    {
        $this->url = $url;
        $this->trace = $trace;
        $this->redirections = $redirections;
        $this->status = $status;
    }

    /**
     * @return String
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return Collection
     */
    public function getTrace(): Collection
    {
        return $this->trace;
    }

    /**
     * @return int
     */
    public function getRedirections(): int
    {
        return $this->redirections;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    public function __toString()
    {
        $str = "Resulted with a {$this->status} status code.
Unshortened URL: {$this->url}
Nb of redirections: {$this->redirections}
Trace:\n";
        foreach ($this->trace as $index => $url) {
            $str .= "{$index}. {$url}\n";
        }
        return $str;
    }


}
